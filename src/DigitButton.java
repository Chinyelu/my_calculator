import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class DigitButton extends JButton {

	public DigitButton(String text, Controller controller, int textSize) {
		super(text);

		setFont(new Font("Arial", Font.PLAIN, textSize));
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.addDigit(text);
			}
		});
	}
}
