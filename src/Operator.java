

public enum Operator {
	ADD("+"), SUBTRACT("-"), MULTIPLY("*"), DIVIDE("/");

	public String text;

	Operator(String text) {
		this.text = text;
	}
}
