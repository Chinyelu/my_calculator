import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class Display {

	private JFrame frame = new JFrame("My Calculator");

	private JTextField bar = new JTextField();

	private JPanel nums = new JPanel();
	private JPanel symbols1 = new JPanel();
	private JPanel botRow = new JPanel();

	public static ArrayList<Double> splitList = new ArrayList<>();

	public ArrayList<String> order = new ArrayList<>();

	public int textSize = 40;

	private OperatorButton add = null;
	private OperatorButton subtract = null;
	private OperatorButton divide = null;
	private OperatorButton multiply = null;

	private DigitButton zero = null;
	private DigitButton one = null;
	private DigitButton two = null;
	private DigitButton three = null;
	private DigitButton four = null;
	private DigitButton five = null;
	private DigitButton six = null;
	private DigitButton seven = null;
	private DigitButton eight = null;
	private DigitButton nine = null;
	private DigitButton decimal = null;

	private JButton equals = null;
	private JButton clear = null;
	private JButton backspace = null;

	public Display() {
	}

	public void createScreen(Controller controller) {

		add = new OperatorButton(Operator.ADD, controller, textSize);
		subtract = new OperatorButton(Operator.SUBTRACT, controller, textSize);
		divide = new OperatorButton(Operator.DIVIDE, controller, textSize);
		multiply = new OperatorButton(Operator.MULTIPLY, controller, textSize);

		zero = new DigitButton("0", controller, textSize);
		one = new DigitButton("1", controller, textSize);
		two = new DigitButton("2", controller, textSize);
		three = new DigitButton("3", controller, textSize);
		four = new DigitButton("4", controller, textSize);
		five = new DigitButton("5", controller, textSize);
		six = new DigitButton("6", controller, textSize);
		seven = new DigitButton("7", controller, textSize);
		eight = new DigitButton("8", controller, textSize);
		nine = new DigitButton("9", controller, textSize);
		decimal = new DigitButton(".", controller, textSize);

		clear = new JButton("CLR");
		backspace = new JButton("<-");
		equals = new JButton("=");

		equals.setFont(new Font("Arial", Font.PLAIN, textSize));
		backspace.setFont(new Font("Arial", Font.PLAIN, textSize));
		clear.setFont(new Font("Arial", Font.PLAIN, textSize));

		// ----------------
		// set up frame
		// ----------------
		frame.setLayout(new FlowLayout());
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setSize(500, 700);

		// ----------------
		// set symbols1
		// ----------------
		symbols1.setLayout(new GridLayout(2, 2));
		symbols1.add(add);
		symbols1.add(subtract);
		symbols1.add(divide);
		symbols1.add(multiply);

		// ----------------
		// set bottom row
		// ----------------
		botRow.setLayout(new GridLayout(0, 4));
		botRow.add(decimal);
		botRow.add(zero);
		botRow.add(equals);
		botRow.add(backspace);

		// ----------------
		// set numbers
		// ----------------
		nums.setLayout(new GridLayout(4, 3));
		nums.add(seven);
		nums.add(eight);
		nums.add(nine);
		nums.add(four);
		nums.add(five);
		nums.add(six);
		nums.add(one);
		nums.add(two);
		nums.add(three);
		nums.add(decimal);
		nums.add(zero);
		nums.add(clear);

		// ----------------
		// positioning
		// ----------------
		frame.setLayout(new BorderLayout());
		frame.add(bar, BorderLayout.NORTH);
		frame.add(nums, BorderLayout.CENTER);
		frame.add(symbols1, BorderLayout.EAST);
		frame.add(equals, BorderLayout.SOUTH);

		// -----------------
		// clear listener
		// -----------------
		clear.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.clear();
			}

		});

		// -----------------
		// equals listener
		// -----------------
		equals.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				controller.compute();
			}
		});

	}

	public void setText(String text) {
		bar.setText(text);
	}

}
