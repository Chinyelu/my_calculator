import java.util.ArrayList;

public class Controller {
	private ArrayList<Operator> operatorSequence = new ArrayList<>();
	private ArrayList<String> numbers = new ArrayList<>();
	private String cache = "";

	private boolean isAnswer = false;

	public Display display = null;

	public void setDisplay(Display display) {
		this.display = display;
	}

	public String getDisplayText() {
		String text = "";
		for (int i = 0; i < numbers.size(); i++) {
			text += numbers.get(i);
			Operator operator = operatorSequence.get(i);
			if (operator != null) {
				text += operator.text;
			}
		}
		text += cache;
		return text;
	}

	public void updateDisplayText() {
		display.setText(getDisplayText());
	}

	public void addDigit(String digit) {
		if (isAnswer) {
			isAnswer = false;
			clear();
		}
		cache += digit;
		updateDisplayText();
	}

	public void addOperator(Operator operator) {
		if (!cache.isEmpty()) {
			operatorSequence.add(operator);
			numbers.add(cache);
			cache = "";
			updateDisplayText();
		}
		else {
			System.out.println("Tried to add operator when there wasn't a previous number");
		}
	}

	public void clear() {
		operatorSequence.clear();
		numbers.clear();
		cache = "";
		updateDisplayText();
	}



	public void compute() {
		numbers.add(cache);
		if (numbers.size() == operatorSequence.size() + 1) {
			Float currentNum = Float.parseFloat(numbers.get(0));
			for (int i = 0; i < operatorSequence.size(); i++) {
				Float nextNum = Float.parseFloat(numbers.get(i+1));
				switch (operatorSequence.get(i)) {
					case ADD: currentNum += nextNum; break;
					case SUBTRACT: currentNum -= nextNum; break;
					case MULTIPLY: currentNum *= nextNum; break;
					case DIVIDE: currentNum /= nextNum; break;
				}
			}
			clear();
			cache = currentNum + "";
			updateDisplayText();
			isAnswer = true;
		}
		else {
			System.out.println("Can't compute that shit yet... Need another number");
		}
	}

}
