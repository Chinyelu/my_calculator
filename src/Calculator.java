
public class Calculator {

	public static void main(String[] args) {
		Display display = new Display();
		Controller controller = new Controller();
		controller.setDisplay(display);
		display.createScreen(controller);
	}

}
