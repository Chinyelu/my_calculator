import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class OperatorButton extends JButton {

	public OperatorButton(Operator operator, Controller controller, int textSize) {
		super(operator.text);

		setFont(new Font("Arial", Font.PLAIN, textSize));
		addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				controller.addOperator(operator);
			}
		});
	}
}
